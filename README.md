# testCollaborativeMindmap

Test concurrent editions of a freeplane mindmap


# Scenario

1. initialise with two branches A and B. B has two sub-branches C and D
1. user1 adds branch E as a sibling of A and B
1. simultaneously
    1. user1 adds branch F as a sibling of C and D and commit
    1. user2 adds branch G as a sibling of C and D and commit
1. user1 removes branch A


## Step 0

![Step 0](testCollaborativeMindmap-step00.png)


## Step 1

![Step 1](testCollaborativeMindmap-step01.png)


## Step 2a

![Step 2a](testCollaborativeMindmap-step02a.png)


## Step 2b

![Step 2b](testCollaborativeMindmap-step02b.png)


## Step 3

![Step 3](testCollaborativeMindmap-step03.png)




